
public class clsShampoo extends clsProducto {
	private int conten;
	
	public clsShampoo(String n, int p, int c) {
		super(n,p);
		this.conten=c;
	}
	public int getconten() {
		return conten;
	}

	public void setconten(int conten) {
		this.conten = conten;
	}
	public String toString() {
		return "Nombre: "+getNomb()+" /// "+"Contenido: "+getconten()+"ml"+" /// "+"Precio: $"+getPrecio();
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
