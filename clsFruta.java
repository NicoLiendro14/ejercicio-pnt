
public class clsFruta extends clsProducto {
	private String unidadPeso;
	
	public clsFruta(String n, int p, String u) {
		super(n,p);
		this.unidadPeso=u;
	}
	public String getUnidadPeso() {
		return this.unidadPeso;
	}
	public void setUnidadPeso(String unidadpeso) {
		this.unidadPeso=unidadpeso;
	}
	public String toString() {
		return "Nombre: "+getNomb()+" /// "+"Precio: $"+getPrecio()+" ///"+" Unidad de venta: "+getUnidadPeso();
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
