
public abstract class clsProducto implements Comparable {
	private String nomb;
	private int precio;
	
	public clsProducto(String n, int p) {
		this.setNomb(n);
		this.setPrecio(p);
	}

	public String getNomb() {
		return nomb;
	}

	public void setNomb(String nomb) {
		this.nomb = nomb;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int compareTo(clsProducto prod1) {
		// TODO Auto-generated method stub
		if(this.precio>prod1.getPrecio())
			return 1;
		else return -1;
	}

	
	
}
