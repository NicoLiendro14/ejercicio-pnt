import java.util.ArrayList;

public class clsLista {
	private ArrayList<clsProducto> lista;
	
	public clsLista() {
		lista=new ArrayList<clsProducto>();
	}
	
	public void cargaLista() {
		clsBebida coca1=new clsBebida("Coca-Cola Zero",20,1.5);
		clsBebida coca2=new clsBebida("Coca-Cola",18,1.5);
		clsShampoo sedal1=new clsShampoo("Shampoo Sedal",19,500);
		clsFruta fruta1=new clsFruta("Frutillas",64,"kilo");
		this.lista.add(coca1);
		this.lista.add(coca2);
		this.lista.add(sedal1);
		this.lista.add(fruta1);
	}
	public void mayor() {
		clsProducto may=lista.get(0);
		for(int i=1;i<lista.size();i++) {
			if(may.compareTo(lista.get(i))<0) {
				may=lista.get(i);
			}
		}
		System.out.println("Producto m�s caro: "+may.getNomb());
	}
	public void menor() {
		clsProducto men=lista.get(0);
		for(int i=1;i<lista.size();i++) {
			if(men.compareTo(lista.get(i))>0) {
				men=lista.get(i);
			}
		}
		System.out.println("Producto m�s barato: "+men.getNomb());
	}
	public void mostrar() {
		for(int i=0;i<lista.size();i++) {
			System.out.println(lista.get(i));
		}
	}
}
