
public class clsBebida extends clsProducto {
	private double litro;
	
	public clsBebida(String n, int p, double d) {
		super(n,p);
		this.litro=d;
	}
	public double getlitro() {
		return litro;
	}

	public void setlitro(float litro) {
		this.litro = litro;
	}
	public String toString() {
		return "Nombre: "+getNomb()+" /// "+"Litros: "+getlitro()+" /// "+"Precio: $"+getPrecio();
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
